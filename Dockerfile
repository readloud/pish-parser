FROM python:alpine

COPY / /parsher/
WORKDIR /parsher
RUN apk -U upgrade
RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "parsher.py" ]
