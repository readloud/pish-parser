# pisher based on SMTP server Postfix

This tool is intended to be a modularized set of scripts to perform network functions in Python3.

Source code based: [PyPhisher](https://github.com/sneakerhax/PyPhisher) sneakerhax | [Python Network Tools](https://github.com/sneakerhax/PNT3)

[![Python 3.7](https://img.shields.io/badge/python-3.7-FADA5E.svg?logo=python)](https://www.python.org/) 
[![Docker](https://img.shields.io/badge/docker-optional-0db7ed.svg?logo=docker)](https://www.docker.com/) 
[![PEP8](https://img.shields.io/badge/code%20style-pep8-red.svg)](https://www.python.org/dev/peps/pep-0008/) 
[![License](https://img.shields.io/badge/license-GPL3-lightgrey.svg)](https://www.gnu.org/licenses/gpl-3.0.en.html) 
[![Twitter](https://img.shields.io/badge/twitter-sneakerhax-38A1F3?logo=twitter)](https://twitter.com/mansz81)

## Introduction
~~~properties

Source code based: PyPhisher | Python Network Tools: sneakerhax

usage: parsher.py [-h] [--ucurl] [--mx] [--dnsresolve] [--dnsreverse] [--whois] --targets TARGETS --server SERVER
                  --port PORT [--username USERNAME] [--password PASSWORD] --html HTML [--url_replace URL_REPLACE]
                  --subject SUBJECT --sender SENDER [--sendto SENDTO] [--start-tls] [--list-sendto LIST_SENDTO]
                  [--attachment ATTACHMENT]

python3 parsher.py --server 127.0.0.1  --port 80 --html README.html --subject README --sendto bb_indo@yahoo.com --sender bb_indo@gmail.com --targets http://144.91.86.150

~~~
## Install

```
python3 -m pip install -r requirements.txt
```

## Running with Docker (Local build)

```
docker build -t pnt3
```

Build Docker image (locally)

```
docker run -it -v ${PWD}/targets.txt:/targets/targets.txt pnt3 --dnsresolve --target /targets/targets.txt
```

Run Docker container with target file

```
docker run -it --entrypoint sh pnt3
```

Run Docker container and drop into bash shell to use scripts

## Running with Docker (From Github Container Registry)

```
docker run -it -v ${PWD}/targets.txt:/targets/targets.txt ghcr.io/sneakerhax/pnt3:latest --dnsresolve --target /targets/targets.txt
```

Run Docker container with target file

```
docker run -it --entrypoint sh ghcr.io/sneakerhax/pnt3:latest
```

Run Docker container and drop into bash shell to use scripts

## Running with Docker (Docker Hub)

```
docker run -it -v ${PWD}/targets.txt:/targets/targets.txt sneakerhax/pnt3:latest --dnsresolve --target /targets/targets.txt
```

Run Docker container with target file

```
docker run -it --entrypoint sh sneakerhax/pnt3:latest
```

Run Docker container and drop into bash shell to use scripts

### Linux SMTP server
SMTP defines how to send mail from one host to another; it is also system independent, which means the sender and receiver can have different operating systems.
SMTP requires only that a server can send straight ASCII text to another server, you can do this by connecting to the server on port 25, which is the standard SMTP port.
Most Linux distros today come with two of the most common implementations of SMTP, which are sendmail and Postfix.
Sendmail is a famous and free mail server, but it has a little complex design and less secure.
The Postfix took mail server implementation one step further; they developed it with security in mind.

### Mail service components

The mail service on any mail server has three components:
* Mail user agent (MUA): this component that the user sees and interacts with like Thunderbird and Microsoft Outlook, these user agents are responsible for reading mail and allowing you to compose mail.
* Mail transport agent (MTA): this component is responsible for getting the mail from one site to another like Sendmail and Postfix.
* Mail delivery agent (MDA): this component is responsible for distributing received messages on the local machine to the appropriate user mailbox like postfix-maildrop and Procmail.

### Setup Email server

We chose the Postfix mail server, which is very popular and common among system administrators today.
Postfix is the default mail server on most modern Linux distros.

First, check if it is installed on your system or not:
~~~
$ rpm -qa | grep postfix
~~~
If not installed, you can install Postfix mail server on Red Hat based distros like this:
~~~
$ sudo apt install postfix -y
~~~
Then start the postfix service and enable it on system startup:
~~~
$ systemctl start postfix
$ systemctl enable postfix
~~~

### Configure Linux mail server
After installing the Postfix mail server, you will need to configure it; you can find most of its configuration files under the /etc/postfix/ directory.

You can find the main configuration for Postfix mail server in /etc/postfix/main.cf file.

### This file contains a lot of options like:

Use this option for specifying the hostname of the mail server. This is the Internet hostname, which Postfix will receive emails on it.

The hostnames could be like mail.example.com, smtp.example.com.
~~~
myhostname = mail.example.com
mydomain = example.com
myorigin = $mydomain
mydestination = $myhostname, localhost.$mydomain, $mydomain, mail.$mydomain, www.$mydomain
mail_spool_directory
mail_spool_directory = /var/spool/mail
mynetworks = 127.0.0.0/8, 192.168.1.0/24
smtpd_banner
inet_protocols = ipv4
~~~
Restart postfix
~~~
$ systemctl reload postfix
$ postfix check
~~~

### Checking the mail queue

To check the mail queue on your Linux mail server, use the following command:
~~~
$ mailq
~~~
This command shows the Postfix mail queue.

If your queue is filled up and the message takes several hours to process, then you should flush the mail queue.
~~~
$ postfix flush
~~~

Now, if you check your mail queue, you should find it empty.

### Test Linux mail server
After configuring the Postfix mail server correctly, you should test your mail server.

The first step is to use a local mail user agent like mailx or mail, which is a symlink to mailx.

Try to send a mail to someone else on the same server, if this works, then send to a remote site.
~~~
$ echo "This is message body" | mailx -s "This is Subject" -r "likegeeks<likegeeks@example.com>" -a /path/to/attachment someone@example.com
~~~

Then try to receive a mail from a remote site.

If you have any problems, check the logs. 

/var/log/mail.log file or as defined in the rsyslogd configuration.

I recommend you to review the Linux Syslog Server for a detailed explanation about logs and how to configure the rsyslogd.

### Secure mailboxes from spam using SpamAssassin
One of the ways to fight spam is to scan the mailboxes by some tool, searching for certain patterns associated with spam.
One of the best solutions is SpamAssassin, which is open-source.

You can install it like this:
~~~
$ susdo apt install spamassassin -y
~~~
Then start the service and enable it at startup:
~~~
$ systemctl start spamassassin
$ systemctl enable spamassassin
~~~
Once you’ve installed it, you can check the configuration in

/etc/mail/spamassassin/local.cf

SpamAssassin determines if an email is spam or not based on the result of the different scripts scores.

In the configuration file, the parameter required_hits 5 indicates that SpamAssassin will mark an email as spam if its score is five or higher.
The report_safe option takes the values 0, 1, or 2. If set to 0, it means email marked as spam is sent as it is, only modifying the headers to show that it is spam.
If it takes the value 1 or 2, SpamAssassin generates a new report message, and it sends the message to the recipient.
The value 1 means the spam message is coded as content message/rfc822, while if the value is 2, that means the message is coded as text/plain content.

Now we need to integrate it into Postfix. The simplest way to do this is probably by using procmail.

We’ll have to create a file, named

/etc/procmailrc

and add the following content:

:0 hbfw
| /usr/bin/spamc

Then we edit Postfix configuration file /etc/postfix/main.cf and change

### mailbox_command
~~~
mailbox_command = /usr/bin/procmail
~~~

### Restart Postfix and SpamAssassin services:
~~~
$ systemctl restart postfix
$ systemctl restart spamassassin
~~~

Filter messages before they enter the Postfix server using Realtime Blackhole Lists (RBLs). That will decrease the load on your mail server and keep your mail server clean.

/etc/postfix/main.cf and change smtpd_recipient_restrictions option 

~~~
strict_rfc821_envelopes = yes
relay_domains_reject_code = 554
unknown_address_reject_code = 554
unknown_client_reject_code = 554
unknown_hostname_reject_code = 554
unknown_local_recipient_reject_code = 554
unknown_relay_recipient_reject_code = 554
unverified_recipient_reject_code = 554
smtpd_recipient_restrictions =
reject_invalid_hostname,
reject_unknown_recipient_domain,
reject_unauth_pipelining,
permit_mynetworks,
permit_sasl_authenticated,
reject_unauth_destination,
reject_rbl_client dsn.rfc-ignorant.org,
reject_rbl_client dul.dnsbl.sorbs.net,
reject_rbl_client list.dsbl.org,
reject_rbl_client sbl-xbl.spamhaus.org,
reject_rbl_client bl.spamcop.net,
reject_rbl_client dnsbl.sorbs.net,
permit
~~~

Restart your postfix server:

~~~
$ systemctl restart postfix
~~~

### Securing SMTP connection
It is better to transfer your SMTP traffic over TLS to protect it from Man In The Middle (MITM) attack.
First, we need to generate the certificate and the key using openssl command:
~~~
$ openssl genrsa -des3 -out mail.key
$ openssl req -new -key mail.key -out mail.csr
$ cp mail.key mail.key.original
$ openssl rsa -in mail.key.original -out mail_secure.key
$ openssl x509 -req -days 365 -in mail_secure.csr -signkey mail_secure.key -out mail_secure.crt
$ cp mail_secure.crt /etc/postfix/
$ cp mail_secure.key /etc/postfix/
~~~
Then add the following option to Postfix configuration file /etc/postfix/main.cf:
~~~
smtpd_use_tls = yes
smtpd_tls_cert_file = /etc/postfix/mail_secure.crt
smtpd_tls_key_file = /etc/postfix/mail_secure.key
smtp_tls_security_level = may
~~~
Restart postfix service:
~~~
$ systemctl restart postfix
~~~

Now, you have to choose the TLS on your client when connecting to the server.

You will receive a warning when you send a mail the first time after changing the setting because the certificate is not signed.

### Using Let’s Encrypt certificates
Let’s Encrypt is a free SSL certificate provider that enables you to encrypt your traffic.

Instead of using self-signed certificates that annoy your users about trusting them, you can use this good solution.

First, install letsencrypt:

~~~
$ apt-get install letsencrypt
~~~
Then run letsencrypt like this:
~~~
$ letsencrypt certonly --standalone -d yourdomain.com
~~~

After answering the prompted questions about the contact email, the email server domain, and the license, everything should be OK now.
The certificates will be in:

/etc/letsencrypt/live/yourdomain.com/

One last thing you have to do, which is making postfix use those certificates, you can use the following commands:
~~~
sudo postconf -e 'smtpd_tls_cert_file = /etc/letsencrypt/live/yourdomain.com/fullchain.pem'
sudo postconf -e 'smtpd_tls_key_file = /etc/letsencrypt/live/yourdomain.com/privkey.pem'
~~~

Don’t forget to replace yourdomain.com with your actual domain.
Restart your postfix server:
~~~
$ systemctl restart postfix
~~~

### POP3 and IMAP protocol basics
So far we’ve seen how SMTP mail server sends and receives emails without problems, but consider the following situations:

Users need local copies of email for offline viewing.
mbox file format is not supported. The mbox format is used by many mail user agents like mailx and mutt.
Users cannot stay connected to a fast network to grab a local copy to read offline.
Some mail servers don’t give access to the shared mail spool directories for security reasons.
To handle these cases, you should use the mail access protocols.

The most common two popular mail access protocols are Post Office Protocol (POP) and Internet Message Access Protocol (IMAP).
The idea behind POP is very simple: A central Linux mail server remains online all the time and receives and store emails for all users. All received emails are queued on the server until a user grabs them.
When a user wants to send an email, the email client relays it through the central Linux mail server via SMTP normally.
Note that the SMTP server and the POP server can be on the same system without any problem. Most servers do this today.
Features like keeping a master copy of a user’s email on the server were missing, which led to the development of IMAP.
By using IMAP, your Linux mail server will support three modes of access:
The online mode is similar to having direct file system access to the Linux mail server.
The offline mode is similar to how POP works, where the client is disconnected from the network except when grabbing an email. In this mode, the server normally does not retain a copy of the email.
The disconnected mode works by allowing users to keep cached copies of their emails, and the server retains a copy of the email.
There are several implementations for IMAP and POP; the most popular one is the Dovecot server, which provides both protocols.

### POP3, POP3S, IMAP, and IMAPS listen on ports 110, 995, 143, and 993 respectively.

### Installing Dovecot

Most Linux distros come with Dovecot preinstalled. However, you can install Dovecot in like this:

~~~
$ apt-get -y install dovecot-imapd dovecot-pop3d
~~~
It will ask you to create self-signed certificates for using IMAP and POP3 over SSL/TLS. Select yes and enter the hostname for your system when prompted.

Then you can run the service and enable it at startup like this:
~~~
$ systemctl start dovecot
$ systemctl enable dovecot
~~~

### Configure Dovecot
The main configuration file for Dovecot is
~~~
/etc/dovecot/dovecot.conf
~~~

Some Linux distros put the configuration under

/etc/dovecot/conf.d/

You can use the following list of the parameters to configure Dovecot:

~~~
protocols = imap pop3 lmtp
~~~

lmtp means local mail transfer protocol.
listen: IP addresses to listen on.

~~~
listen = *, ::
The asterisk means all ipv4 interfaces and :: means all ipv6 interfaces
userdb: user database for authenticating users.
userdb {
driver = pam
}
passdb: password database for authenticating users.
passdb {
driver = passwd
}
mail_location: this entry in /etc/dovecot/conf.d/10-mail.conf file:
mail_location = mbox:~/mail:INBOX=/var/mail/%u
~~~

### Secure Dovecot
Dovecot comes with generic SSL certificates and key files. Look at this file:

/etc/dovecot/conf.d/10-ssl.conf
~~~
ssl_cert = </etc/pki/dovecot/certs/dovecot.pem
ssl_key = </etc/pki/dovecot/private/dovecot.pem
~~~

When a user tries to connect to dovecot server, it will show a warning because the certificates are not signed, you can purchase a certificate from a certificate authority if you want.

Or if you go with Let’s Encrypt certificates, you can point to them instead:
~~~
ssl_cert = </etc/letsencrypt/live/yourdomain.com/fullchain.pem
ssl_key = </etc/letsencrypt/live/yourdomain.com/privkey.pem
~~~

dovecot server ports in your iptables firewall by adding iptables rules for ports 110, 995, 143, 993, 25.

Or if you are using firewalld, you can do the following:
~~~
$ firewall-cmd --permanent --add-port=110/tcp --add-port=995/tcp
$ firewall-cmd --permanent --add-port=143/tcp --add-port=993/tcp
$ firewall-cmd --reload
~~~

And again, for troubleshooting, you check the log files /var/log/messages, /var/log/maillog, and /var/log/mail.log files.
Linux mail server is one of the easiest servers to work with, especially the Postfix mail server.
Thank you.
